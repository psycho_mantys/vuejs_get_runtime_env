FROM nginx:stable as production-stage

#COPY --from=build /app/dist /usr/share/nginx/html
COPY . /usr/share/nginx/html

COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

