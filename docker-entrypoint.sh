#!/bin/sh

if [ ! -f /usr/share/nginx/html/config.js ] ; then
	envsubst < /usr/share/nginx/html/config.js.tmpl > /usr/share/nginx/html/config.js
fi

exec "$@"

