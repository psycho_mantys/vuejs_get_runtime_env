Vue get envs on runtime working
==================================


```
docker build -t vue_test .

docker run -d -it -p 9999:80 --name vue_test -e VUE_APP_API_BASE_URL=https://push.weni.ai/ vue_test
curl http://localhost:9999/config.js
docker container rm -f vue_test

docker run -d -it -p 9999:80 --name vue_test -e VUE_APP_API_BASE_URL=https://flows.weni.ai/ vue_test
curl http://localhost:9999/config.js
docker container rm -f vue_test
```
